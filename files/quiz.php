<?
ini_set('display_errors', 1);
session_start();
require_once('admin/config.php');
include_once('admin/inc/functions.php');
require_once('admin/classes/DBPDO.class.php');
include_once('models/quiz.php');
include_once('controllers/quiz.php');

?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<? echo ADMIN_URL ?>assets/js/jquery-ui.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<? echo ADMIN_URL ?>assets/js/bootstrap.min.js"></script>
<script src="<? echo ADMIN_URL ?>assets/js/actions.js"></script>

<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/style.css">
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/ionicons/css/ionicons.min.css">
<link rel="shortcut icon" type="image/x-icon" href="<? echo ADMIN_URL ?>favicon.ico"/>

<link href="<? echo ADMIN_URL ?>assets/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
<script src="<? echo ADMIN_URL ?>assets/js/jquery.circliful.js"></script>
<script src="<? echo ADMIN_URL ?>ckeditor/ckeditor.js"></script>

<title>QUIZ</title>
</head>
<body style="color:white; background-color:white;">

<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12"><h1 style="color:black" class="text-center">Tests</h1></div>
  </div>
  <?
$quizes = new Quizes();
$allquizes = $quizes->getQuizes();
  ?>
  <div class="row">
    <div class="col-xs-12">
      <ul style="color:black;">
        <? foreach($allquizes as $item){ ?>
        <li><strong>&bull; <? echo html_entity_decode($item['title']); ?></strong> <a href="take_test.php?quiz_id=<? echo $item['id'] ?>">Take Test</a></li>
        <? } ?>
      </ul>
    </div>
  </div>

</div>
</body>
</html>