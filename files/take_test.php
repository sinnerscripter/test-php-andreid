<?
ini_set('display_errors', 1);
session_start();
require_once('admin/config.php');
include_once('admin/inc/functions.php');
require_once('admin/classes/DBPDO.class.php');
include_once('models/quiz.php');
include_once('models/questions.php');
include_once('controllers/quiz.php');
include_once('controllers/test.php');

if(!isset($_GET['quiz_id'])){
  exit('NO quiz id defined');

}else{
$current_quiz_id=$_GET['quiz_id'];
$quizes=new Quizes();
$questions=new Questions();
$currentquiz=$quizes->getQuiz($current_quiz_id);
$allquestions=explode(',',$currentquiz['allquestions']);

if(empty($currentquiz)){
  exit('NO such quiz id in DB');
}else{


?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<? echo ADMIN_URL ?>assets/js/jquery-ui.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<? echo ADMIN_URL ?>assets/js/bootstrap.min.js"></script>
<script src="<? echo ADMIN_URL ?>assets/js/actions.js"></script>

<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/style.css">
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/ionicons/css/ionicons.min.css">
<link rel="shortcut icon" type="image/x-icon" href="<? echo ADMIN_URL ?>favicon.ico"/>

<link href="<? echo ADMIN_URL ?>assets/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
<script src="<? echo ADMIN_URL ?>assets/js/jquery.circliful.js"></script>
<script src="<? echo ADMIN_URL ?>ckeditor/ckeditor.js"></script>

<title>Take test</title>
</head>
<body style="color:black; background-color:white;">

<div class="container-fluid">
  <div class="row">
    <div class="col-xs-12"><h1 style="color:black" class="text-center">QUIZ: <? echo html_entity_decode($currentquiz['title']) ?></h1></div>
  </div>

  <div class="row">
    <div class="col-xs-12">

      <form role="form" class="form" method="post">
          
          <div class="form-group col-lg-6 col-lg-offset-3">
            <br><br>
            <label for="name_surname" style="min-width:150px;">Insert Your Name/Surname:</label>
            <input type="text" class="form-control" id="name_surname" name="name_surname">
          </div>
          <div class="col-lg-12"><br><br><h2 class="text-center">Your Questions:</h2><br><br></div>
          <br>
          <? foreach($allquestions as $quest){
            $questionid=$questions->getQuestion($quest);
            
          ?>
          <div class="form-group col-lg-12" style="border-bottom:1px solid #ccc;">
            <label for="name_surname" style="min-width:150px;"><? echo html_entity_decode($questionid['title']) ?></label>
            <div class="checkbox">
              <label><input  name="resp_<? echo $questionid['id'] ?>[]" type="checkbox" value="1"><? echo html_entity_decode($questionid['response_1']) ?></label>
            </div>
            <div class="checkbox">
              <label><input  name="resp_<? echo $questionid['id'] ?>[]" type="checkbox" value="2"><? echo html_entity_decode($questionid['response_2']) ?></label>
            </div>
            <div class="checkbox">
              <label><input  name="resp_<? echo $questionid['id'] ?>[]" type="checkbox" value="3"><? echo html_entity_decode($questionid['response_3']) ?></label>
            </div>
          </div>

          <? } ?>

          <div class="form-group col-xs-12 paddinger center">
            <button type="submit" class="btn btn-default" name="submit_quiz">Submit</button>
          </div>
      </form>






    </div>
  </div>

</div>
</body>
</html>
<? }} ?>