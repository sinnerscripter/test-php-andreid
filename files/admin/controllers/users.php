<?

if(isset($_GET['model'])){
	include_once('models/'.$_GET['controller'].'.php');
}else{
	include_once('models/'.$_GET['controller'].'.php');
}



if($_SESSION['user']==''){
 header("Location:".ADMIN_URL."login");
}else{
	//do stuff
	$users=new Users();
	$active_user =$users->getUser('25');
	$name=$active_user['name'];
	$role=$active_user['role'];
	$image=$active_user['image_thumb'];
	$status=$active_user['status'];
	$status_name=$users->GetStatusName($status);



	$all_users=$users->getUsers();

	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add_user'])) {
		//add

		$pass=$_POST['pass'];

		$username=$_POST['username'];
		//$username=test_input($_POST["username"]);

		if(!filter_var($username, FILTER_VALIDATE_EMAIL)){
			$error='Invalid Email Address';
			exit(header("Location:".ADMIN_URL."users/add/?error=".$error.""));
		}
		$name=$_POST['name'];
		$role=$_POST['role'];
		$status=$_POST['status'];
		$phone=$_POST['phone'];
		if(!preg_match('/^[+]?(\d{1,2})?[\s.-]?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/', $phone)){
			$error='Invalid Phone NUmber';
			exit(header("Location:".ADMIN_URL."users/add/?error=".$error.""));
		}
					
		function rand_string($length) {
			$str="";
			$chars = "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			$size = strlen($chars);
			for($i = 0;$i < $length;$i++) {
				$str .= $chars[rand(0,$size-1)];
			}
			return $str;
		}
		$p_salt = rand_string(20); 
		$site_salt="cucurukujerry"; 
		$salted_hash = hash('sha256', $pass.$site_salt.$p_salt);
		$password=$salted_hash;
		$users->addUser($username, $password, $p_salt, $role, $name, $phone, $status);
		header("Location:".ADMIN_URL."users");
	}


	if (isset($_GET['action']) && $_GET['action']== 'delete' && isset($_GET['id'])) {
		//delete
		$id=$_GET['id'];
		if($image!=''){unlink($image);}
		$users->deleteUser($id);
		header("Location:".ADMIN_URL."users");
	}

	if (isset($_GET['model']) && $_GET['model']== 'edit' && isset($_GET['id'])) {
		$edit=new Users();
		$edit_user =$users->getUser($_GET['id']);
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edit_user']) && isset($_GET['id'])) {
		$id=$_GET['id'];
		$name=$_POST['name'];
		$role=$_POST['role'];
		$status=$_POST['status'];
		$edit->UpdateUser($name, $role, $status, $id);
		header("Location:".ADMIN_URL."users");
	}





}



if(isset($_GET['model'])){
	include_once('views/'.$_GET['controller'].'.'.$_GET['model'].'.php');
}else{
	include_once('views/'.$_GET['controller'].'.php');
}
?>