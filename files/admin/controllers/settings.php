<?

if(isset($_GET['model'])){
	include_once('models/'.$_GET['controller'].'.php');
}else{
	include_once('models/'.$_GET['controller'].'.php');
}




if($_SESSION['user']==''){
 header("Location:".ADMIN_URL."login");
}else{
	//do stuff
	$settings=new Settings();
	$get_settings=$settings->getSettings('1');
	$title_en=html_entity_decode($get_settings['title_en']);
	$title_ro=html_entity_decode($get_settings['title_ro']);
	$title_ru=html_entity_decode($get_settings['title_ru']);
	$meta_description_en=html_entity_decode($get_settings['meta_description_en']);
	$meta_description_ro=html_entity_decode($get_settings['meta_description_ro']);
	$meta_description_ru=html_entity_decode($get_settings['meta_description_ru']);
	$meta_keywords_en=html_entity_decode($get_settings['meta_keywords_en']);
	$meta_keywords_ro=html_entity_decode($get_settings['meta_keywords_ro']);
	$meta_keywords_ru=html_entity_decode($get_settings['meta_keywords_ru']);
	$google_analytics_code=html_entity_decode($get_settings['google_analytics_code']);
	$maintenance=html_entity_decode($get_settings['maintenance']);


	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edit_settings'])) {
		
		$title_en=htmlentities($_POST['title_en']);
		$title_ro=htmlentities($_POST['title_ro']);
		$title_ru=htmlentities($_POST['title_ru']);
		$meta_description_en=htmlentities($_POST['meta_description_en']);
		$meta_description_ro=htmlentities($_POST['meta_description_ro']);
		$meta_description_ru=htmlentities($_POST['meta_description_ru']);
		$meta_keywords_en=htmlentities($_POST['meta_keywords_en']);
		$meta_keywords_ro=htmlentities($_POST['meta_keywords_ro']);
		$meta_keywords_ru=htmlentities($_POST['meta_keywords_ru']);
		$google_analytics_code=htmlentities($_POST['google_analytics_code']);
		$maintenance=$_POST['maintenance'];

		$settings->UpdateSettings($title_en, $title_ro, $title_ru, $meta_description_en, $meta_description_ro, $meta_description_ru, $meta_keywords_en, $meta_keywords_ro, $meta_keywords_ru, $google_analytics_code, $maintenance,'1');
		header("Location:".ADMIN_URL."settings");
	}





}


if(isset($_GET['model'])){
	include_once('views/'.$_GET['controller'].'.'.$_GET['model'].'.php');
}else{
	include_once('views/'.$_GET['controller'].'.php');
}
?>