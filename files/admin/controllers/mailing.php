<?

if(isset($_GET['model'])){
	include_once('models/'.$_GET['controller'].'.php');
}else{
	include_once('models/'.$_GET['controller'].'.php');
}




if($_SESSION['user']==''){
 header("Location:".ADMIN_URL."login");
}else{
	//do stuff
	$invitation=new Invitation();
	$all_invitations=$invitation->getInvitations();
	$pending_invitations=$invitation->getPendingInvitations();
	$sent_invitations=$invitation->getSentInvitations();
	
	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add_bulk'])) {
		$pending_status='1';
		$sent_status='0';
		$ok=1;
		$datetime=date('Y-m-d H:i:s');
		if(isset($_POST['bulklist']) && $_POST['bulklist']!=''){$bulklist=str_replace(array("\n"), ';', $_POST['bulklist']);}else{$ok=0;}
		if($ok=='1'){
			$bk= explode(';', $bulklist);
			foreach($bk as $item){
				$vk=explode(', ', $item);
				$memail=htmlentities($vk[0]);
				$mename=htmlentities(trim($vk[1]));
				if(empty($invitation->getInvitationByEmail($vk[0]))){
					$invitation->addInvitation($memail, $mename, $pending_status, $sent_status);
				}
			}
			$success2='<div style="color:green; text-align:center;">E-mail List was scheduled for mailing.</div>';
		}//end ok=1
		header("Location:".ADMIN_URL."mailing");
	}


	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add_mailing'])) {
		$pending_status='1';
		$sent_status='0';
		$ok=1;
		$datetime=date('Y-m-d H:i:s');
		if(isset($_POST['email']) && $_POST['email']!=''){$email=$_POST['email'];}else{$ok=0;}
		if(isset($_POST['namesurname']) && $_POST['namesurname']!=''){$namesurname=htmlentities($_POST['namesurname']);}else{$ok=0;}
		if($ok=='1'){
			
			if(empty($invitation->getInvitationByEmail($_POST['email']))){
				$invitation->addInvitation($email, $namesurname, $pending_status, $sent_status);
				$success='<div style="color:green; text-align:center;">E-mail <strong>'.$email.'</strong> was scheduled for mailing.</div>';
				header("Location:".ADMIN_URL."mailing");
			}else{
				$success='<div style="color:red; text-align:center;">E-mail <strong>'.$email.'</strong> already exists.</div>';
			}

		}//end if ok =1
		
	}


	if (isset($_GET['action']) && $_GET['action']== 'delete_mail' && isset($_GET['id'])) {
		//delete
		$id=$_GET['id'];

		
		$invitation->deleteInvitation($id);
		header("Location:".ADMIN_URL."mailing");
	}

	if (isset($_GET['action']) && $_GET['action']== 'reque_mail' && isset($_GET['id'])) {
		//delete
		$id=$_GET['id'];

		
		$invitation->requeInvitation($id);
		header("Location:".ADMIN_URL."mailing");
	}


	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action']) && $_POST['action']=='update_pending') {
		$array	= $_POST['arrayorder'];
		$count = 1;
		foreach ($array as $idval) {
			$invitation->UpdatePendingOrder($count, $idval);
			$count ++;	
		}
	}



}


if(isset($_GET['model'])){
	include_once('views/'.$_GET['controller'].'.'.$_GET['model'].'.php');
}else{
	include_once('views/'.$_GET['controller'].'.php');
}
?>