<?

if(isset($_GET['model'])){
	include_once('models/'.$_GET['controller'].'.php');
}else{
	include_once('models/'.$_GET['controller'].'.php');
}



if($_SESSION['user']==''){
 header("Location:".ADMIN_URL."login");
}else{
	//do stuff
	$translations=new Translation();
	$all_translations =$translations->getTranslations();


	if (isset($_GET['model']) && $_GET['model']== 'edit' && isset($_GET['id'])) {
		//get translation
		$id=$_GET['id'];
		$get_translation = $translations->getTranslation($id);
	}


	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['add_translation'])) {
		$qued_text=$_POST['qued_text'];
		$trans_en=$_POST['trans_en'];
		$trans_ro=$_POST['trans_ro'];
		$trans_ru=$_POST['trans_ru'];
		$translations->addTranslation($qued_text, $trans_en, $trans_ro, $trans_ru);
		header("Location:".ADMIN_URL."translation");
	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['edit_translation']) && isset($_GET['id'])) {
		$id=$_GET['id'];
		$qued_text=$_POST['qued_text'];
		$trans_en=$_POST['trans_en'];
		$trans_ro=$_POST['trans_ro'];
		$trans_ru=$_POST['trans_ru'];
		$translations->UpdateTranslation($qued_text, $trans_en, $trans_ro, $trans_ru,$id);
		header("Location:".ADMIN_URL."translation");
	}

	if (isset($_GET['action']) && $_GET['action']== 'delete' && isset($_GET['id'])) {
		$id=$_GET['id'];
		$translations->deleteTranslation($id);
		header("Location:".ADMIN_URL."translation");
		
	}





}



if(isset($_GET['model'])){
	include_once('views/'.$_GET['controller'].'.'.$_GET['model'].'.php');
}else{
	include_once('views/'.$_GET['controller'].'.php');
}
?>