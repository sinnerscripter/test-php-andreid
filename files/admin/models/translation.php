<?

Class Translation {
	public function getTranslations() {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $getrows = $database->getRows("SELECT * FROM translations ORDER BY id DESC", array());
            $database->Disconnect();
            return $getrows;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function addTranslation($qued_text, $trans_en, $trans_ro, $trans_ru) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO translations (qued_text, trans_en, trans_ro, trans_ru) VALUES (?, ?, ?, ?)", array($qued_text, $trans_en, $trans_ro, $trans_ru));
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getTranslation($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
           
            $getrow = $database->getRow("SELECT * FROM translations WHERE id=?", array($id));
            $database->Disconnect();
            return $getrow;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdateTranslation($qued_text,$trans_en,$trans_ro,$trans_ru,$id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE translations SET qued_text = ?, trans_en = ?, trans_ro = ?, trans_ru = ? WHERE id = ?", array($qued_text, $trans_en, $trans_ro, $trans_ru, $id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function deleteTranslation($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $deleterow = $database->deleteRow("DELETE FROM translations WHERE id = ?", array($id));
            $database->Disconnect();
            return $deleterow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    
}



?>