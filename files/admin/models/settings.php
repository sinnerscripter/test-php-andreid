<?
Class Settings {

	public function getSettings($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
           
            $getrow = $database->getRow("SELECT * FROM settings WHERE id=?", array($id));
            $database->Disconnect();
            return $getrow;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdateSettings($title_en,$title_ro,$title_ru,$meta_description_en,$meta_description_ro,$meta_description_ru,$meta_keywords_en,$meta_keywords_ro,$meta_keywords_ru,$google_analytics_code,$maintenance,$id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE settings SET title_en = ?, title_ro = ?, title_ru = ?, meta_description_en = ?, meta_description_ro = ?, meta_description_ru = ?, meta_keywords_en = ?, meta_keywords_ro = ?, meta_keywords_ru = ?, google_analytics_code = ?, maintenance = ? WHERE id = ?", array($title_en, $title_ro, $title_ru, $meta_description_en, $meta_description_ro, $meta_description_ru, $meta_keywords_en, $meta_keywords_ro, $meta_keywords_ru, $google_analytics_code, $maintenance, $id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }



}
?>