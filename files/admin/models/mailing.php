<?
Class Invitation{

	
	public function getInvitations() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `invitations` ORDER BY `datetime` DESC", array());
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

    public function getInvitationByEmail($email) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
           
            $getrow = $database->getRow("SELECT * FROM invitations WHERE email=?", array($email));
            $database->Disconnect();
            return $getrow;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getPendingInvitations() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `invitations` WHERE `pending`=? ORDER BY `sort` ASC", array('1'));
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

    public function getSentInvitations() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `invitations` WHERE `sent`=? ORDER BY `datetime` DESC", array('1'));
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

    

    public function deleteInvitation($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $deleterow = $database->deleteRow("DELETE FROM `invitations` WHERE id = ?", array($id));
            $database->Disconnect();
            return $deleterow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function addInvitation($email, $namesurname, $pending_status, $sent_status) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO `invitations` (`email`, `namesurname`, `pending`, `sent`) VALUES (?, ?, ?, ?)", array($email, $namesurname, $pending_status, $sent_status)); 
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function requeInvitation($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE `invitations` SET `pending`=?, `sent`=? WHERE id = ?", array('1', '0',  $id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdatePendingOrder($count, $idval) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE `invitations` SET sort = ? WHERE id = ?", array($count, $idval));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    

}
?>