<?
Class Quizes{

	
	public function getQuizes() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `quizes` ORDER BY `id` DESC", array());
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

    public function getQuiz($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
           
            $getrow = $database->getRow("SELECT * FROM quizes WHERE id=?", array($id));
            $database->Disconnect();
            return $getrow;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    

    public function deleteQuiz($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $deleterow = $database->deleteRow("DELETE FROM `quizes` WHERE id = ?", array($id));
            $database->Disconnect();
            return $deleterow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function addQuiz($title, $allquestions) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO `quizes` (`title`, `allquestions`) VALUES (?, ?)", array($title, $allquestions, )); 
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getQuestions() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `questions` ORDER BY `id` ASC", array());
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

/*    public function UpdateQuiz($count, $idval) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE `invitations` SET sort = ? WHERE id = ?", array($count, $idval));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }
*/
    

}
?>