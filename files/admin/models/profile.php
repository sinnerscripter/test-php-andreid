<?

Class Profile {
	public function getUser($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");	
        try{
            $getrow=$database->getRow("SELECT * FROM users WHERE id =?", array($id));
            $database->Disconnect();
            return $getrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdateUser($username,$name,$id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE users SET username = ?, name = ? WHERE id = ?", array($username,$name,$id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdatePassword($password,$id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE users SET password = ? WHERE id = ?", array($password,$id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdateProfileImage($image_thumb,$id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE users SET image_thumb = ? WHERE id = ?", array($image_thumb,$id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    
    

    
}



?>