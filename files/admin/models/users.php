<?

Class Users {
	public function getUser($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $getrow = $database->getRow("SELECT * FROM users WHERE id =?", array($id));
            $database->Disconnect();
            return $getrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getUsers() {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $getrows = $database->getRows("SELECT * FROM users ORDER BY id DESC", array());
            $database->Disconnect();
            return $getrows;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }


    public function addUser($username, $password, $psalt, $role, $name, $phone, $status) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO users (username, password, psalt, role, name, phone, status) VALUES (?, ?, ?, ?, ?, ?, ?)", array($username, $password, $psalt, $role, $name, $phone, $status));
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function deleteUser($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $deleterow = $database->deleteRow("DELETE FROM users WHERE id = ?", array($id));
            $database->Disconnect();
            return $deleterow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getStatusName($status) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $getrow = $database->getRow("SELECT * FROM statuses WHERE value =?", array($status));
            $database->Disconnect();
            return $getrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getStatusTranslation($status_name) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $getrow = $database->getRow("SELECT * FROM translations WHERE qued_text =?", array($status_name));
            $database->Disconnect();
            return $getrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function UpdateUser($name,$role,$status, $id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $updaterow=$database->updateRow("UPDATE users SET name = ?, role = ?, status = ? WHERE id = ?", array($name, $role, $status, $id));
            $database->Disconnect();
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }


    
}



?>