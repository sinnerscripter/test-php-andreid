﻿/*
 Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/
CKEDITOR.addTemplates("default", {
    imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
    templates: [{
        title: "Bootstrap Row with 2 colums",
        image: "template_bootstrap_1-2.jpg",
        description: "One row with 2 columns of 50% width.",
        html: '<div class="row"><div class="col-md-6"><h2>First Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div><div class="col-md-6"><h2>Second Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div></div>'
    }, {
        title: "Bootstrap Row with 3 colums",
        image: "template_bootstrap_1-3.jpg",
        description: "One row with 3 columns of 33,33% width.",
        html: '<div class="row"><div class="col-md-4 col-sm-6"><h2>First Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div><div class="col-md-4 col-sm-6"><h2>Second Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div><div class="col-md-4 col-sm-6"><h2>Third Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div></div>'
    }, {
        title: "Bootstrap Row with 4 colums",
        image: "template_bootstrap_1-4.jpg",
        description: "One row with 4 columns of 25% width.",
        html: '<div class="row"><div class="col-md-3 col-sm-6"><h2>First Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div><div class="col-md-3 col-sm-6"><h2>Second Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div><div class="col-md-3 col-sm-6"><h2>Third Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div><div class="col-md-3 col-sm-6"><h2>Fourth Column Title</h2><p><img src="'+CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/")+'responsive_image.jpg" alt="" class="img img-responsive"></p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sollicitudin condimentum ante, ac fermentum eros mollis volutpat. Praesent et nibh nibh. Aliquam maximus velit sit amet consectetur aliquam.</p></div></div>'
    }, {
        title: "Image and Title",
        image: "template1.gif",
        description: "One main image with a title and text that surround the image.",
        html: '<h3><img src=" " alt="" style="margin-right: 10px" height="100" width="100" align="left" />Type the title here</h3><p>Type the text here</p>'
    }, {
        title: "Strange Template",
        image: "template2.gif",
        description: "A template that defines two colums, each one with a title, and some text.",
        html: '<table cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr><td style="width:50%"><h3>Title 1</h3></td><td></td><td style="width:50%"><h3>Title 2</h3></td></tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></table><p>More text goes here.</p>'
    }, {
        title: "Text and Table",
        image: "template3.gif",
        description: "A title with some text and a table.",
        html: '<div style="width: 80%"><h3>Title goes here</h3><table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1"><caption style="border:solid 1px black"><strong>Table title</strong></caption><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table><p>Type the text here</p></div>'
    }]
});