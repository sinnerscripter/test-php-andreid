<?


?>

<?
date_default_timezone_set('Europe/Chisinau');

function getStatus($status) {
	if($status=='1'){
   		return "Active";
	}
	elseif($status=='0'){
		return "Suspended";
	}
	elseif($status=='2'){
		return "Pending Validation";
	}
	elseif($status=='3'){
		return "Valid";
	}
	elseif($status=='4'){
		return "Stopped";
	}
	elseif($status=='5'){
		return "Verificaţi Contul Disponibil";
	}
	elseif($status=='6'){
		return "Not Valid";
	}
	elseif($status=='7'){
		return "Finalizat";
	}
}
function randomstring($length) {
		 $str="";
		$chars = "0123456789";
		$size = strlen($chars);
		for($i = 0;$i < $length;$i++) {
			$str .= $chars[rand(0,$size-1)];
		}
		return $str;					 
}

function randombannerid($length) {
					  $str="";
					  $chars = "subinsblogabcdefghijklmanopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
					  $size = strlen($chars);
					  for($i = 0;$i < $length;$i++) {
					   $str .= $chars[rand(0,$size-1)];
					  }
					  return $str;
}

function getip(){
	if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
		return $ip;
	} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		return $ip;
	} else {
		$ip = $_SERVER['REMOTE_ADDR'];
		return $ip;
	}
}

function getdatetime(){
	$datetime= (new \DateTime())->format('d-m-Y H:i:s');
	return $datetime;
}
function getsignupdate(){
	$datetime= (new \DateTime())->format('d-m-Y');
	return $datetime;
}


/*YANDEX TRANSLATE FUNCTION*/
function translate($text, $from, $to)
{
    $api = 'trnsl.1.1.20170305T141122Z.dd454e2673591f4f.b9d93803cb526e3feb0ed91a4ce441d43f56560c'; // TODO: Get your key from https://tech.yandex.com/translate/
    $url = file_get_contents('https://translate.yandex.net/api/v1.5/tr.json/translate?key=' . $api . '&lang=' . $from . '-' . $to . '&text=' . $text);
    $json = json_decode($url);
    return $json->text[0];
}




/*CHECKS if multiple value of characters exist in string*/
function strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
                $res = strpos($haystack, $needle, $offset);
                if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        return min($chr);
}
/* USAGE
if (strposa($string, $array, 1)) {
    echo 'true';
} else {
    echo 'false';
}

*/


function trad($qued_text) {
        $database = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', ''.DB_USER.'', ''.DB_PASS.'');;  
        try{
            $del = $database->prepare("SELECT * FROM translations WHERE qued_text =?");
            $del->execute(array($qued_text));
            $count = $del->rowCount();
            if ($count!='0') {
            	while($g=$del->fetch()){
            		return $g['trans_'.$_SESSION['lang'].''];
            	}
            }else{
            	return $qued_text;
            }

			$database->Disconnect();
            
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

}

function validate_email($email){
	$ch = curl_init("https://apineo.tk/email-validation/?email=".$email."");  
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$json = curl_exec($ch);
	curl_close($ch);
	$validationResult = json_decode($json, true);
	if($validationResult["result"]=="valid"){
		return true;
	}else{
		return false;
	}
	
}

/*
//translate countries
$database = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', ''.DB_USER.'', ''.DB_PASS.'');
 $del44 = $database->prepare("SELECT * FROM country WHERE `nicename_ro`=? ORDER BY `id` ASC LIMIT 20");
 $del44->execute(array(''));
 while($r44=$del44->fetch()){
 	$nicename_ro=translate(urlencode($r44['nicename']), 'en', 'ro');
 	$nicename_ru=translate(urlencode($r44['nicename']), 'en', 'ru');
 	$iii=$r44['id'];
 	$del55 = $database->prepare("UPDATE `country` SET `nicename_ro`=?, `nicename_ru`=? WHERE `id`=?");
 	$del55->execute(array($nicename_ro, $nicename_ru, $iii));
 }*/


?>
