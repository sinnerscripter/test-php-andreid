<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<? echo ADMIN_URL ?>assets/js/jquery-ui.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/bootstrap.min.css">
<!-- Optional theme -->
<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/bootstrap-theme.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="<? echo ADMIN_URL ?>assets/js/bootstrap.min.js"></script>
<script src="<? echo ADMIN_URL ?>assets/js/actions.js"></script>

<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/style.css">
<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,500,700&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<? echo ADMIN_URL ?>assets/css/ionicons/css/ionicons.min.css">
<link rel="shortcut icon" type="image/x-icon" href="<? echo ADMIN_URL ?>favicon.ico"/>

<link href="<? echo ADMIN_URL ?>assets/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
<script src="<? echo ADMIN_URL ?>assets/js/jquery.circliful.js"></script>
<script src="<? echo ADMIN_URL ?>ckeditor/ckeditor.js"></script>

<title><? echo ADMIN_TITLE ?></title>
</head>
<body>

<div class="container-fluid">