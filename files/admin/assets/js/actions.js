// JavaScript Document

$(document).ready(function(){
	
    /*START ALT HINTS TO ELEMENTS*/
	$('[data-toggle="tooltip"]').tooltip(); 
	/*END ALT HINTS TO ELEMENTS*/
	
	/*FUNCTIONALITY FORM MOBILE MENU START*/	
	$('.menu-toggle-mobile').click(function(){
		$('.parent-menu').toggle('slow');
	});
	/*FUNCTIONALITY FORM MOBILE MENU END*/
	
	/*FIX FOR RIGHT SIDE DESKTOP VERSION START*/
	$('.menu-toggle-desktop').click(function(){
		$('.left').toggleClass('short');
		$('.right').toggleClass('right-long');
		$('.parent-menu').toggleClass('menu-margin-top');
		$('.left .user').toggle();
		$('.parent-menu li ul').hide('slow');
	});
	/*FIX FOR RIGHT SIDE DESKTOP VERSION END*/
	
	/*START SHOW RIGHT ARROW IF CHILD MENU EXISTS*/
	$('.parent-menu li').each(function( index ) {
		if ($(this).children('ul').length) {
				 	
		}else{
			$(this).children('a').children('i').hide();
			$('.parent-menu li').not(this).children('ul').children('li').children('a').children('i').show();
		}
	});
	/*END SHOW RIGHT ARROW IF CHILD MENU EXISTS*/
	
	/*FUNCTIONALITY FOR DESKTOP MENU START*/	
	$('.parent-menu li').each(function( index ) {
  		$(this).click(function(){
			$(this).children('.parent-menu li ul').toggle('slow');
			$( '.parent-menu li').not(this).children('.parent-menu li ul').hide('slow');
			$(this).children('a').children('i').toggleClass('glyphicon-menu-down');
			$( '.parent-menu li').not(this).children('a').children('i').removeClass('glyphicon-menu-down');
			
			/*START FIX FOR SHORT MENU*/
			$('.left').removeClass('short');
			$('.right').removeClass('right-long');
			$('.parent-menu').removeClass('menu-margin-top');
			$('.left .user').show('slow');
			
			/*END FIX FOR SHORT MENU*/
			
		});
	});
	/*FUNCTIONALITY FOR DESKTOP MENU END*/	
	
	/* START FUNCTIONALITY FOR MODAL PASSING VALUES for edit_evaluation_marks */
	$('.editor').click(function(){
		$('#edit_evaluation_marks .modal-title').text($(this).parent().children('td:nth-child(2)').text());
		$('#edit_evaluation_marks #user_id').val($(this).parent().attr('data-user-id'));
		$('#edit_evaluation_marks #discipline_id').val($(this).parent().attr('data-discipline-id'));
		
		if(/*$(this).parent().children('td').length > 7 && */ $(this).parent().attr('data-discipline-id')=='1' && $(this).parent().attr('data-section')=='zi'){
			
		$('#edit_evaluation_marks .modal-body #atestation_one').val($(this).parent().children('td:nth-child(3)').text());
		$('#edit_evaluation_marks .modal-body #atestation_two').val($(this).parent().children('td:nth-child(4)').text());
		$('#edit_evaluation_marks .modal-body #project').val(changetext($(this).parent().children('td:nth-child(5)').text()));
		$('#edit_evaluation_marks .modal-body #exam').val($(this).parent().children('td:nth-child(6)').text());
		/*SCROLL PAGE to position AFTER SOME EDITS*/
		document.cookie='topposition='+ $(document).scrollTop() +'';
		}else if ($(this).parent().attr('data-discipline-id')=='2' && $(this).parent().attr('data-section')=='zi'){
		
		$('#edit_evaluation_marks .modal-body #atestation_one').val($(this).parent().children('td:nth-child(3)').text());
		$('#edit_evaluation_marks .modal-body #atestation_two').val($(this).parent().children('td:nth-child(4)').text());
		$('#edit_evaluation_marks .modal-body #project').parent().hide();
		$('#edit_evaluation_marks .modal-body #exam').val($(this).parent().children('td:nth-child(5)').text());
	/*SCROLL PAGE to position AFTER SOME EDITS*/
		document.cookie='topposition='+ $(document).scrollTop() +'';
		}else if($(this).parent().attr('data-discipline-id')=='1' && $(this).parent().attr('data-section')=='f/r'){
			$('#edit_evaluation_marks .modal-body #atestation_one').val($(this).parent().children('td:nth-child(3)').text());
			$('#edit_evaluation_marks .modal-body #project').val(changetext($(this).parent().children('td:nth-child(4)').text()));
			$('#edit_evaluation_marks .modal-body #exam').val($(this).parent().children('td:nth-child(5)').text());
			
		}else if($(this).parent().attr('data-discipline-id')=='2' && $(this).parent().attr('data-section')=='f/r'){
			$('#edit_evaluation_marks .modal-body #atestation_one').val($(this).parent().children('td:nth-child(3)').text());
			$('#edit_evaluation_marks .modal-body #atestation_two').parent().hide();
			$('#edit_evaluation_marks .modal-body #project').parent().hide();
			$('#edit_evaluation_marks .modal-body #exam').val($(this).parent().children('td:nth-child(4)').text());
		}
		
		
		
	});
	

	
	
	/* END FUNCTIONALITY FOR MODAL PASSING VALUES for edit_evaluation_marks */
	
	/* START FUNCTIONALITY FOR MODAL PASSING VALUES for edit_current_marks */
	$('.editor').click(function(){
		$('#edit_current_marks .modal-title').text($(this).parent().children('td:nth-child(2)').text());
		$('#edit_current_marks #user_id').val($(this).parent().attr('data-user-id'));
		$('#edit_current_marks #discipline_id').val($(this).parent().attr('data-discipline-id'));
		
		$('#edit_current_marks .modal-body #mark_one').val($(this).parent().children('td:nth-child(3)').text());
		$('#edit_current_marks .modal-body #mark_two').val($(this).parent().children('td:nth-child(4)').text());
		$('#edit_current_marks .modal-body #mark_three').val($(this).parent().children('td:nth-child(5)').text());
		$('#edit_current_marks .modal-body #mark_four').val($(this).parent().children('td:nth-child(6)').text());
		$('#edit_current_marks .modal-body #mark_five').val($(this).parent().children('td:nth-child(7)').text());
		$('#edit_current_marks .modal-body #mark_six').val($(this).parent().children('td:nth-child(8)').text());
		$('#edit_current_marks .modal-body #mark_seven').val($(this).parent().children('td:nth-child(9)').text());
		$('#edit_current_marks .modal-body #mark_eight').val($(this).parent().children('td:nth-child(10)').text());
		/*SCROLL PAGE to position AFTER SOME EDITS*/
		document.cookie='topposition='+ $(document).scrollTop() +'';
	});
	
	/* END FUNCTIONALITY FOR MODAL PASSING VALUES for edit_current_marks */
	/*START of function for changing "nu a sustinut", "n/p", "Nu a efectuat" to numbers*/
	function changetext(vl){
		if(vl=='Nu a susţinut'){vl='4';return vl;}
		else if(vl=='Nu a efectuat'){vl='0';return vl;}
		else if (vl=='n/p'){vl='0';return vl;}
		else{return vl;}
		
	}
	/*END of function for changing "nu a sustinut", "n/p", "Nu a efectuat" to numbers*/
	
	/* START FUNCTIONALITY FOR MODAL PASSING VALUES for edit_lab_marks */
	$('.editor').click(function(){
		
		$('#edit_lab_marks .modal-title').text($(this).parent().children('td:nth-child(2)').text());
		$('#edit_lab_marks #user_id').val($(this).parent().attr('data-user-id'));
		$('#edit_lab_marks #discipline_id').val($(this).parent().attr('data-discipline-id'));
		$('#edit_lab_marks .modal-body #lab_one').val(changetext($(this).parent().children('td:nth-child(3)').text()));
		$('#edit_lab_marks .modal-body #lab_two').val(changetext($(this).parent().children('td:nth-child(4)').text()));
		$('#edit_lab_marks .modal-body #lab_three').val(changetext($(this).parent().children('td:nth-child(5)').text()));
		$('#edit_lab_marks .modal-body #lab_four').val(changetext($(this).parent().children('td:nth-child(6)').text()));
		$('#edit_lab_marks .modal-body #lab_five').val(changetext($(this).parent().children('td:nth-child(7)').text()));
		/*SCROLL PAGE to position AFTER SOME EDITS*/
		document.cookie='topposition='+ $(document).scrollTop() +'';
		
		
	});
	/* END FUNCTIONALITY FOR MODAL PASSING VALUES for edit_lab_marks */
	/*START SCROLL PAGE to position AFTER SOME EDITS*/
	function getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i=0; i<ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') c = c.substring(1);
				if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
			}
			return "";
	}
		
		var a = document.cookie;
		var tp =getCookie('topposition');
		//alert(getCookie('topposition'));
		
		
			if (tp != "") {
				$(document).scrollTop(tp);
			} else {
				document.cookie='topposition='+ $(document).scrollTop() +'';
			}
		
	/*END SCROLL PAGE to position AFTER SOME EDITS*/
	
	
	
	
	/* START FUNCTIONALITY FOR MODAL PASSING VALUES for edit_student_profile */
	$('.editor').click(function(){
		$('#edit_student_profile .modal-title').text($(this).parent().children('td:nth-child(2)').text());
		$('#edit_student_profile #user_id').val($(this).parent().attr('data-user-id'));
		$('#edit_student_profile #group_id').val($(this).parent().attr('data-group-id'));
		$('#edit_student_profile #new_group_id').val($(this).parent().attr('data-group-id'))	
		$('#edit_student_profile .modal-body #name').val($(this).parent().children('td:nth-child(2)').text());
		/*SCROLL PAGE to position AFTER SOME EDITS*/
		document.cookie='topposition='+ $(document).scrollTop() +'';
	});
	/* END FUNCTIONALITY FOR MODAL PASSING VALUES for edit_student_profile */
	
	

	
    
	/*START SORTABLE STUDENT LIST*/	
	$("#sortable-menu tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_menu'; 
				$.post("menu", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  


	$( "#sortable-menu tbody" ).disableSelection();

	$("#sortable-pending tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_pending'; 
				$.post("mailing", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-pending tbody" ).disableSelection();

	$("#sortable-partners tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_partners'; 
				$.post("module-partners", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-partners tbody" ).disableSelection();

	$("#sortable-moviecategories tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_moviecategories'; 
				$.post("movie-categories", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-moviecategories tbody" ).disableSelection();

	$("#sortable-industries tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_industries'; 
				$.post("industry", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-industries tbody" ).disableSelection();

	$("#sortable-juries tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_juries'; 
				$.post("jury", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-juries tbody" ).disableSelection();

	$("#sortable-teams tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_teams'; 
				$.post("team", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-teams tbody" ).disableSelection();

	$("#sortable-sections tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_sections'; 
				$.post("sections", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-sections tbody" ).disableSelection();

	$("#sortable-moviegenres tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_moviegenres'; 
				$.post("movie-genres", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-moviegenres tbody" ).disableSelection();

	$("#sortable-moviestatuses tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_moviestatuses'; 
				$.post("movie-statuses", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-moviestatuses tbody" ).disableSelection();

	$("#sortable-movieawards tbody").sortable({update: function() {
				var order = $(this).sortable("serialize") + '&action=update_movieawards'; 
				$.post("movie-awards", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-movieawards tbody" ).disableSelection();

	$("#sortable-moviescreens tbody").sortable({update: function() {
				var order = $(this).sortable("serialize") + '&action=update_moviescreens'; 
				$.post("movie-screens", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-moviescreens tbody" ).disableSelection();

	$("#sortable-banerlogos tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_logos'; 
				$.post("module-banner", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-banerlogos tbody" ).disableSelection();


	$("#sortable-submenu tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_submenu'; 
				$.post("menu", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  


	$( "#sortable-submenu tbody" ).disableSelection();

	$("#sortable-submenu2 tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&action=update_submenu2'; 
				$.post("menu", order, function(theResponse){
					//console.log(order);
					//console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-submenu2 tbody" ).disableSelection();

	

	$("#sortable-quality tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_sortable_quality.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-quality tbody" ).disableSelection();

	$("#sortable-colors tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_sortable_colors.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-colors tbody" ).disableSelection();

	$("#sortable-materiale tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_sortable_materiale.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-materiale tbody" ).disableSelection();

	$("#sortable-werecommend tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_sortable_werecommend.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-werecommend tbody" ).disableSelection();

	$("#sortable-slider-homepage tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_sortable_slider_homepage.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-slider-homepage tbody" ).disableSelection();
	
	$("#sortable-cat2 tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_sortable_cat2.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  
	$( "#sortable-cat2 tbody" ).disableSelection();




	/*END SORTABLE STUDENT LIST*/		
	
	$('.atestation_one').circliful();
	$('.atestation_two').circliful();
	$('.projects').circliful();
	$('.exam').circliful();
	$('.final').circliful();
	
	/*START show group list when vizitator is selected in add new user*/
		$('#reg_user #gr').parent('div').hide();
		$('#reg_user #role').change(function(){
			if($(this).val()=='visitor'){
				$('#reg_user #gr').parent('div').show();
			}else{
				$('#reg_user #gr').parent('div').hide();
			}
		});

	/*END show group list when vizitator is selected in add new user*/
	
	
	
	/*START check image before upload*/
	/*var _URL = window.URL || window.webkitURL;
	$("#image_thumb").change(function(e) {
		var file, img;
		if ((file = this.files[0])) {
			img = new Image();
			img.onload = function() {
				//alert(this.width + " " + this.height);
				if(this.width == this.height){
					//alert('width = height');
					$('.modal-footer').children('button[type="submit"]').css({'display':'inline-block'});
				}else{
					alert('Imaginea trebuie să aibă dimensiuni egale (exemplu: 180px × 180px sau 200px × 200px)');
					$('.modal-footer').children('button[type="submit"]').css({'display':'none'});
				}
			};
			img.onerror = function() {
				alert( "Fişierul nu este o imagine: " + file.type);
				$('.modal-footer').children('button[type="submit"]').css({'display':'none'});
				
			};
			img.src = _URL.createObjectURL(file);
		}
	});*/
	/*END check image before upload*/


	/*START SORTABLE CATEGORY LIST*/	
		$("#sortable tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_categories_order.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  


	$( "#sortable tbody" ).disableSelection();
	/*END SORTABLE CATEGORY LIST*/
	/*START SORTABLE PRODUCT LIST*/	
		$("#sortable-products tbody").sortable({update: function() {
				
				var order = $(this).sortable("serialize") + '&update=update'; 
				$.post("update_products_order.php", order, function(theResponse){
					console.log(order);
					console.log(theResponse);
				}); 															 
		}});								  


	$( "#sortable-products tbody" ).disableSelection();
	/*END SORTABLE PRODUCT LIST*/

$('#sortable-menu').children('tbody').children('tr:first-child').children('td').each(function(index){
	//alert($(this).width());
	$('#sortable-menu table tr').children('td:nth-child('+(index+1)+')').width($(this).width());
});


/*START Filter translations by Item in ADMIN*/
$('.table tr td').addClass('dtd');
$('#d_id').on('keyup', function(){
		$('.table tr').show().removeClass('good');
		var tosearch = $(this).val().toLowerCase();
		$('.table .dtd').each(function(){
			var eachtext=$(this).text().toLowerCase();
			//console.log(eachtext);
			if(eachtext.indexOf(tosearch)>=0){
				//if found
				$(this).parent('tr').addClass('good');
				$('.table tr').hide();
				$('.table tr.good').show();
			}else{
				$('.table tr').hide();
				$('.table tr.good').show();
			}
		});
});
/*END Filter translations by Item in ADMIN*/
$('#gaz').click(function(){
	$.post(window.location, {st: '1'}, function(data) {
	  location.reload();
	});
});

$('#tormoz').click(function(){
	$.post(window.location, {st: '0'}, function(data) {
	  location.reload();
	});
});

	
});
