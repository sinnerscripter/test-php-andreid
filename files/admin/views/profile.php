<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">

		<div class="row paddinger">
			
			<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Profile Settings</h4></div>
				<div class="panel-body">
				
				
				<div class="center user paddinger" style="text-align:center; "><span><img data-toggle="tooltip" data-title="Profile Image" data-placement="bottom"  src="<?if($image!=''){echo ADMIN_URL.$image;}else{echo ADMIN_URL.'uploads/noimage.jpg';}  ?>" alt="<? echo $name; ?>" class="img-responsive img-circle" style="float:none; margin-left:auto; margin:auto;" /></span><a style="cursor:pointer;" data-toggle="tooltip" data-title="Profile Image" data-placement="bottom"><? echo $name; ?></a><br></div>
				
				<form role="form" class="form" method="post" enctype="multipart/form-data">
					<div class="form-group col-lg-6">
						<label for="name" style="min-width:150px;">Name / Surname:</label>
						<input type="hidden" name="action" value="edit_user_profile">
						<input type="hidden" name="user_id" value="<? echo $id; ?>">
						<input type="text" class="form-control" id="name" name="name"  value="<? echo $name; ?>">
				  	</div>
					<div class="form-group col-lg-6">
						<label for="username" style="min-width:150px;">E-mail (Username):</label>
						<input type="text" class="form-control" id="username" name="username"  value="<? echo $username; ?>" <? if($role!='admin'){?> readonly disabled <? } ?>>
				  	</div>
					<div class="form-group col-lg-6">
						<label for="pass" style="min-width:150px;">Password:</label>
						<input type="password" class="form-control" id="pass" name="pass" >
				  	</div>
				  	<div class="form-group col-lg-6">
						<label for="role" style="min-width:150px;">Role:</label>
						<input type="text" class="form-control" id="role" name="role"  placeholder="<? if($role=='admin'){ echo 'Administrator'; }elseif($role=='editor'){echo 'Editor';} ?>" readonly disabled>
				  	</div>
				  	<div class="form-group col-lg-6">
						<label for="pass" style="min-width:150px;">Profile Image:</label>
						<input type="file" class="form-control" id="image_thumb" name="image_thumb" >
				  	</div>
					<div class="form-group col-xs-12 paddinger center">
						<button type="submit" class="btn btn-default">Save</button>
					</div>
				</form>
				
				
				
				</div>
			</div>
			</div>
			
		</div>



	</div>
</div>

<?
include_once('footer.php'); 
?>