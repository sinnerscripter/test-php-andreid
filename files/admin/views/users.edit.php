<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">
		


		<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Edit User</h4></div>
				<div class="panel-body">
				
				
				<form role="form" class="form" method="post">
					
					<div class="form-group col-lg-6">
						<label for="name" style="min-width:150px;">Name/Surname:</label>
						<input type="hidden" name="action" value="register_new_user">
						<input type="text" class="form-control" id="name" name="name" value="<? echo $edit_user['name'] ?>">
				  	</div>
					<div class="form-group col-lg-6 ">
						<label for="role" style="min-width:150px;">Role:</label>
						<select id="role" name="role" class="form-control" style="min-width:150px;">
							<option value="admin" <? if($edit_user['role']=='admin'){echo 'selected';} ?> >Administrator</option>
							<option value="editor" <? if($edit_user['role']=='editor'){echo 'selected';} ?> >Editor</option>
							<option value="visitor" <? if($edit_user['role']=='visitor'){echo 'selected';} ?> >Visitor</option>
						</select>
				  	</div>
				  	<div class="form-group col-lg-6 ">
						<label for="status" style="min-width:150px;">Status:</label>
						<select id="status" name="status" class="form-control" style="min-width:150px;">
							<option value="1" <? if($edit_user['status']=='1'){echo 'selected';} ?> >Active</option>
							<option value="0" <? if($edit_user['status']=='0'){echo 'selected';} ?> >Blocked</option>
						</select>
				  	</div>
                    
					<div class="form-group col-xs-12 paddinger center">
						<button type="submit" class="btn btn-default" name="edit_user">Edit User</button>
					</div>
				</form>
				</div>
			</div>
			</div>



		</div>
	</div>
</div>

<?
include_once('footer.php'); 
?>