<?
include_once('head.php');
//this queries the controller
//echo 'login view .<br>';
?>
<div class="row loginer paddinger">
<div class="col-lg-4 col-md-6 col-sm-8 col-lg-offset-4 col-md-offset-3 col-sm-offset-2 center" style="padding-left:0; padding-right:0; background:#fff;">
	<div class="logo">
		<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 width="68px" height="68px" viewBox="0 0 68 68" style="enable-background:new 0 0 68 68;" xml:space="preserve">
<path style="fill:#FFFFFF;" d="M45.793,28.64c0.286-0.268,0.551-0.446,0.786-0.553c0.237-0.105,0.442-0.153,0.688-0.155
	c0.368,0.006,0.676,0.102,1.09,0.387c0.649,0.438,1.423,1.429,2.165,2.737h3.997c-0.268-0.542-0.541-1.077-0.829-1.592
	c-0.718-1.279-1.504-2.462-2.52-3.413c-0.508-0.473-1.083-0.888-1.74-1.188c-0.654-0.3-1.396-0.477-2.163-0.474
	c-1.173-0.006-2.261,0.414-3.126,1.033c-1.521,1.094-2.534,2.688-3.466,4.411c-1.372,2.587-2.504,5.58-3.724,7.806
	c-0.601,1.107-1.225,2.01-1.787,2.539c-0.28,0.269-0.537,0.444-0.761,0.548c-0.226,0.104-0.415,0.146-0.642,0.147
	c-0.423-0.005-0.735-0.109-1.136-0.383c-0.695-0.466-1.521-1.606-2.277-3.123c-1.157-2.272-2.196-5.262-3.582-7.821
	c-0.702-1.282-1.494-2.477-2.557-3.432c-0.533-0.475-1.139-0.885-1.826-1.176c-0.687-0.291-1.452-0.453-2.245-0.452
	c-1.181-0.005-2.279,0.412-3.152,1.034c-1.534,1.102-2.542,2.706-3.457,4.437c-1.349,2.6-2.442,5.603-3.634,7.831
	c-0.588,1.111-1.201,2.012-1.757,2.54c-0.276,0.267-0.53,0.44-0.753,0.544c-0.226,0.103-0.417,0.147-0.654,0.149v3.543
	c0.008,0,0.016,0,0.024,0c1.161,0,2.238-0.426,3.085-1.048c1.499-1.108,2.475-2.705,3.375-4.433
	c1.325-2.592,2.418-5.589,3.626-7.811c0.596-1.107,1.221-2.003,1.792-2.533c0.286-0.268,0.55-0.444,0.79-0.553
	c0.241-0.107,0.455-0.157,0.715-0.158c0.494,0.006,0.865,0.127,1.3,0.409c0.755,0.482,1.607,1.608,2.385,3.117
	c1.188,2.259,2.222,5.234,3.553,7.779c0.675,1.275,1.425,2.463,2.439,3.423c0.507,0.477,1.088,0.895,1.755,1.192
	c0.666,0.3,1.416,0.468,2.191,0.467c1.166,0.006,2.244-0.422,3.093-1.043c1.494-1.1,2.485-2.688,3.405-4.407
	c1.354-2.581,2.484-5.57,3.717-7.79C44.586,30.07,45.217,29.172,45.793,28.64z"/>
<path style="fill:#FFFFFF;" d="M59.593,40.735c-0.565-0.392-1.247-1.252-1.916-2.391L55.9,34.802h-7.579l0,0h-2.148l-1.623,3.543
	h9.112c0.208,0.415,0.418,0.825,0.637,1.223c0.707,1.275,1.472,2.455,2.463,3.407c0.496,0.473,1.058,0.892,1.704,1.196
	c0.645,0.303,1.379,0.483,2.139,0.481v-3.543C60.272,41.104,59.995,41.018,59.593,40.735z"/>
</svg>
	</div>
	<h4 class="titler"><? echo ADMIN_TITLE ?></h4>
	<div class="clearfix"></div>
</div>
<div class="col-lg-4 col-md-6 col-sm-8 col-lg-offset-4 col-md-offset-3 col-sm-offset-2 center" id="login-box">
<h3 class="paddinger">Log In</h3>
<form method="post" action="<? echo ADMIN_URL ?>login" role="form">
  
  <div class="form-group">
    <label for="mail" class="sr-only">Email address:</label>
    <input type="mail" class="form-control" id="mail" name="mail" placeholder="Email Address">
	<span class="glyphicon ion-ios-email-outline"></span>
  </div>
  <div class="form-group">
    <label for="pass" class="sr-only">Password:</label>
    <input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
	<span class="glyphicon ion-ios-locked-outline"></span>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
<br>

</div>

</div>
<?

include_once('footer.php');
?>