<div class="col-md-2 left" style="padding-left:0; position:relative;">
		<div class="menu-toggle-desktop center" data-toggle="tooltip" data-placement="bottom" title="Colapse/Expand Menu"><i class="ion-navicon"></i></div>
		<div class="menu-toggle-mobile center" data-toggle="tooltip" data-placement="bottom" title="Show/Hide Menu"><i class="ion-navicon"></i></div>
		<div class="center user paddinger" >
			<a href="<? echo ADMIN_URL.'profile' ?>"><span><img data-toggle="tooltip" data-title="Profile Image" data-placement="bottom"  src="<? if($image!=''){echo ADMIN_URL.$image;}else{echo ADMIN_URL.'uploads/noimage.jpg';} ?>" alt="<? echo $name ?>" class="img-responsive img-circle" /></span></a>
			<br>Hello, <a style="cursor:pointer;" data-toggle="tooltip" data-title="Change Profile Image" data-placement="bottom" href="<? echo ADMIN_URL.'profile' ?>"><? echo $name; ?></a>
		</div>
		<ul class="parent-menu">
			
			<? if($role=='admin'){ ?>
			<li <? if (strpos($_SERVER['REQUEST_URI'],'home') !== false){ echo 'class="active"';}?>><a href="<? echo ADMIN_URL ?>home"><span class="glyphicon ion-ios-flag-outline center"></span> Home <i class="glyphicon glyphicon-menu-right"></i></a></li>

		
			<li <? if (strpos($_SERVER['REQUEST_URI'],'quiz') !== false){ echo 'class="active"';}?>><a href="<? echo ADMIN_URL ?>quiz"><span class="glyphicon ion-ios-email-outline center"></span> Add Quiz <i class="glyphicon glyphicon-menu-right"></i></a></li>

			<li <? if (strpos($_SERVER['REQUEST_URI'],'questions') !== false){ echo 'class="active"';}?>><a href="<? echo ADMIN_URL ?>questions"><span class="glyphicon ion-ios-email-outline center"></span> Add Questions <i class="glyphicon glyphicon-menu-right"></i></a></li>
			
			<li <? if (strpos($_SERVER['REQUEST_URI'],'users') !== false){ echo 'class="active"';}?>><a href="<? echo ADMIN_URL ?>users"><span class="glyphicon ion-ios-person-outline center"></span> Users <i class="glyphicon glyphicon-menu-right"></i></a></li>
			
			<li <? if (strpos($_SERVER['REQUEST_URI'],'translation') !== false){ echo 'class="active"';}?>><a href="<? echo ADMIN_URL ?>translation"><span class="glyphicon ion-ios-pricetags-outline center"></span> Translation <i class="glyphicon glyphicon-menu-right"></i></a></li>
			<? /*<li <? if (strpos($_SERVER['REQUEST_URI'],'settings') !== false){ echo 'class="active"';}?>><a href="<? echo ADMIN_URL ?>settings"><span class="glyphicon ion-ios-gear-outline center"></span> Settings <i class="glyphicon glyphicon-menu-right"></i></a></li> */ ?>

			<? } ?>
		</ul>
	</div>