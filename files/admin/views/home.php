<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">

		

		<div class="row paddinger">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h4 class="paddinger">Chestionare Completate</h4>
						
					</div>
					<div class="panel-body">
						
                    	<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th class="center">Pending E-mails</th>
										<th class="center">Sent E-mails</th>
									</tr>
								</thead>
								<tbody>
									
									<tr>
										<td class="center" style="color:red;"><? echo sizeof($pending_invitations); ?></td>
										<td class="center" style="color:green;"><? echo sizeof($sent_invitations); ?></td>
									</tr>
									
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

<?
include_once('footer.php'); 
?>