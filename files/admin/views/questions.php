<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">
		


		<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Add Question</h4></div>
				<div class="panel-body">
				<div style="color:red;"><? if(isset($_GET['error'])){echo $_GET['error'];} ?> </div>
				
				<form role="form" class="form" method="post">
					
					<div class="form-group col-lg-12">
						<label for="title" style="min-width:150px;">Question:</label>
						<input type="text" class="form-control" id="title" name="title">
				  	</div>
					<div class="form-group col-lg-12">
						<label for="response_1" style="min-width:150px;">Response 1:</label>
						<input type="text" class="form-control" id="response_1" name="response_1">
				  	</div>
				  	<div class="form-group col-lg-12">
						<label for="response_2" style="min-width:150px;">Response 2:</label>
						<input type="text" class="form-control" id="response_2" name="response_2">
				  	</div>
					<div class="form-group col-lg-12">
						<label for="response_3" style="min-width:150px;">Response 3:</label>
						<input type="text" class="form-control" id="response_3" name="response_3">
				  	</div>
					<div class="form-group col-lg-12">
						<label for="correct" style="min-width:150px;">Correct Responses:</label>
						<select name="correct[]" multiple="multiple">
							<option value="1">Response 1</option>
							<option value="2">Response 2</option>
							<option value="3">Response 3</option>
						</select>
					  	
                    </div>
					<div class="form-group col-xs-12 paddinger center">
						<button type="submit" class="btn btn-default" name="add_question">Add Question</button>
					</div>
				</form>
				</div>
			</div>
			</div>



		</div>
	</div>
</div>

<?
include_once('footer.php'); 
?>