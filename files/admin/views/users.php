<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">
		


		<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Users</h4>
					<? if($role=='admin'){ ?><a href="<? echo ADMIN_URL ?>users/add/"><i class="ion-ios-plus red"></i> Register User</a><? } ?>
				</div>
				<div class="panel-body">
					
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th class="center">Status</th>
									<th class="center">Profile Image</th>
									<th class="center">Name/Surname</th>
									<th class="center">Phone</th>
									<th class="center">E-mail (Username)</th>
									<th class="center">Role</th>
									<? if($role=='admin'){?>
									<th class="center">Edit</th>
									<th class="center">Delete</th>
									<? } ?>
								</tr>
							</thead>
							<tbody>
								<?
								
								foreach ($all_users as $r7) {
									$st=new Users();
									$status_name=$st->getStatusName($r7['status']);
									$status_name=$status_name['name'];
									$status_name=$st->getStatusTranslation($status_name);
									$status_name=$status_name['trans_'.$lang.''];
									if($r7['id']!=$_SESSION['user']){
								?>
								<tr>
									<td class="center"><? echo $status_name; ?></td>
								<td><div class="user"><img src="<?if($r7['image_thumb']!=''){echo ADMIN_URL; echo $r7['image_thumb'];}else{echo ADMIN_URL; echo 'uploads/noimage.jpg';}  ?>" alt="<? echo $r7['name']; ?>" class="img-responsive img-circle" style="cursor:auto;"></div></td>
									<td><? echo $r7['name']; ?></td>
									<td><? echo $r7['phone']; ?></td>
									<td class="center"><? echo $r7['username']; ?></td>
									<td class="center"><? if($r7['role']=='admin'){ echo 'Administrator'; }elseif($r7['role']=='editor'){echo 'Editor';}elseif($r7['role']=='visitor'){echo 'Visitor';} ?></td>
									<? if($role=='admin'){?>
									<td class="center editor"><a href="<? echo ADMIN_URL ?>users/edit?id=<? echo $r7['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit User"><i class="ion-ios-compose-outline"></i></a></td>
									<td class="center remover"><a href="<? echo ADMIN_URL ?>users?action=delete&amp;id=<? echo $r7['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete User Profile" onClick="return confirm('Are you sure you want to delete this user profile?');"><i class="ion-ios-close-empty"></i></td>
									<? }} ?>
								</tr>
								<? } ?>
							</tbody>
						</table>
					</div>
				
				</div>
			</div>
			</div>



		</div>
	</div>
</div>

<?
include_once('footer.php'); 
?>