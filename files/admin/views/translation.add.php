<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">
			

			<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Add Translation</h4></div>
				<div class="panel-body">
				
				
				
				<form role="form" class="form-inline" method="post" enctype="multipart/form-data">
					<div class="form-group col-xs-12 paddinger">
						<label for="qued_text" style="min-width:150px;">Text (Item):<span class="red">*</span></label>
						<input type="text" class="form-control" id="qued_text" name="qued_text" value="" style="width:100%" required>
				  	</div>
				  	<div class="form-group col-lg-4 paddinger">
						<label for="trans_en" style="min-width:150px;">Traducere (EN):</label>
						<input type="text" class="form-control" id="trans_en" name="trans_en" value="" style="width:100%">
				  	</div>
					<div class="form-group col-lg-4 paddinger">
						<label for="trans_ro" style="min-width:150px;">Traducere (RO):</label>
						<input type="text" class="form-control" id="trans_ro" name="trans_ro" value="" style="width:100%">
				  	</div>
					<div class="form-group col-lg-4 paddinger">
						<label for="trans_ru" style="min-width:150px;">Traducere (RU):</label>
						<input type="text" class="form-control" id="trans_ru" name="trans_ru" value="" style="width:100%">
				  	</div>
					
				  	<div class="clearfix"></div>
				  	
					<div class="form-group col-xs-12 paddinger center">
						<button type="submit" class="btn btn-default" name="add_translation">Add Translation</button>
					</div>
				</form>
				</div>
			</div>
			</div>

		</div>


	</div>
</div>

<?
include_once('footer.php'); 
?>