<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">

		<div class="row paddinger" id="students">
			<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Site Settings</h4></div>
				<div class="panel-body">
								
								<form method="post">
								<div class="form-group col-lg-12 paddinger">
				                    <div class="checkbox">
										<label><input type="checkbox" value="1" name="maintenance" <? if($maintenance=='1'){echo 'checked="checked"';} ?>> Maintenance Mode</label><br><br>
									</div>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="title_en" >Site Title (En)<span class="red">*</span></label>
									<textarea name="title_en" rows="4" id="title_en" class="form-control" required><? echo $title_en; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="title_ro" >Site Title (Ro)<span class="red">*</span></label>
									<textarea name="title_ro" rows="4" id="title_ro" class="form-control" required><? echo $title_ro; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="title_ru" >Site Title (Ru)<span class="red">*</span></label>
									<textarea name="title_ru" rows="4" id="title_ru" class="form-control" required><? echo $title_ru; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="meta_description_en" >Meta Description (En)</label>
									<textarea name="meta_description_en" rows="4" id="meta_description_en" class="form-control"><? echo $meta_description_en; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="meta_description_ro" >Meta Description (Ro)</label>
									<textarea name="meta_description_ro" rows="4" id="meta_description_ro" class="form-control"><? echo $meta_description_ro; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="meta_description_ru" >Meta Description (Ru)</label>
									<textarea name="meta_description_ru" rows="4" id="meta_description_ru" class="form-control"><? echo $meta_description_ru; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="meta_keywords_en" >Meta Keywords (En)</label>
									<textarea name="meta_keywords_en" rows="4" id="meta_keywords_en" class="form-control"><? echo $meta_keywords_en; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="meta_keywords_ro" >Meta Keywords (Ro)</label>
									<textarea name="meta_keywords_ro" rows="4" id="meta_keywords_ro" class="form-control"><? echo $meta_keywords_ro; ?></textarea>
								</div>
								<div class="form-group col-sm-4 col-xs-12">
									<label for="meta_keywords_ru" >Meta Keywords (Ru)</label>
									<textarea name="meta_keywords_ru" rows="4" id="meta_keywords_ru" class="form-control"><? echo $meta_keywords_ru; ?></textarea>
								</div>
								<div class="form-group col-xs-12">
									<label for="google_analytics_code" >Google Analytics Tracking Code</label>
									<textarea name="google_analytics_code" rows="8" id="google_analytics_code" class="form-control"><? echo $google_analytics_code; ?></textarea>
								</div>
								
								<div class="form-group col-xs-12 text-center">
									<input type="submit" name="edit_settings" value="Save" class="btn btn-default">
								</div>
								</form>
							
				
				</div>
			</div>
			</div>
			
			</div>

	</div>
</div>

<?
include_once('footer.php'); 
?>