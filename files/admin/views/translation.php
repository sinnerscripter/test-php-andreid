<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">

			<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">All Translations</h4><a href="<? echo ADMIN_URL ?>translation/add/"><i class="ion-ios-plus red"></i> Add Translation</a></div>
				<div class="panel-body">

					<div class="row">
						<div class="text-center col-xs-12">
							<form class="form form-inline">
								<div class="form-group">
									<label for="d_id">Filter by text: </label> <input type="text" id="d_id" class="form-control" style="min-width:200px;">
								</div>
							</form>
						</div>
					</div>
					<br>
				
					<div class="table-responsive">
						<table class="table table-striped" >
							<thead>
								<tr>
									<th>Item</th>
									<th>English</th>
									<th>Romanian</th>
									<th>Russian</th>
									<th class="center">Edit</th>
									<th class="center">Delete</th>
								</tr>
							</thead>
							<tbody>
								<?
								foreach($all_translations as $r7){
								 
								?>
								<tr id="arrayorder_<?php echo $r7['id'] ?>">
									<td style="color:#aeaeae;"><? echo html_entity_decode($r7['qued_text']) ?></td>
									<td class=" <? if(html_entity_decode($r7['trans_en'])=='No Translation'){echo' red';} ?>"><? echo html_entity_decode($r7['trans_en']) ?></td>
									<td class=" <? if(html_entity_decode($r7['trans_ro'])=='No Translation'){echo' red';} ?>"><? echo html_entity_decode($r7['trans_ro']) ?></td>
									<td class=" <? if(html_entity_decode($r7['trans_ru'])=='No Translation'){echo' red';} ?>"><? echo html_entity_decode($r7['trans_ru']) ?></td>
									<td class="center editor"><a href="<? echo ADMIN_URL ?>translation/edit?id=<? echo $r7['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Edit Element"><i class="ion-ios-compose-outline"></i></a></td>
									<td class="center remover"><a href="<? echo ADMIN_URL ?>translation?action=delete&amp;id=<? echo $r7['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete Element" onClick="return confirm('Are you sure you want to delete?');"><i class="ion-ios-close-empty"></i></td>
								</tr>
								<?  } ?>
							</tbody>
						</table>
					</div>
				
				</div>
			</div>
			</div>
			
			
			

			

		</div>
	</div>
</div>

<?
include_once('footer.php'); 
?>