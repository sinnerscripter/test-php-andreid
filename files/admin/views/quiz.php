<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">
		


		<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Add Quiz</h4></div>
				<div class="panel-body">
				<div style="color:red;"><? if(isset($_GET['error'])){echo $_GET['error'];} ?> </div>
				
				<form role="form" class="form" method="post">
					
					<div class="form-group col-lg-12">
						<label for="title" style="min-width:150px;">Quiz Title:</label>
						<input type="text" class="form-control" id="title" name="title">
				  	</div>
					<div class="form-group col-lg-12">
						<label for="allquestions" style="min-width:150px;">Choose Questions:</label>
						<select name="allquestions[]" multiple="multiple">
							<? foreach ($allquestions as $item) { ?>
								<option value="<? echo $item['id'] ?>"><? echo $item['title'] ?></option>
							<? } ?>
						</select>
					  	
                    </div>
					<div class="form-group col-xs-12 paddinger center">
						<button type="submit" class="btn btn-default" name="add_quiz">Add Quiz</button>
					</div>
				</form>
				</div>
			</div>
			</div>



		</div>
	</div>
</div>

<?
include_once('footer.php'); 
?>