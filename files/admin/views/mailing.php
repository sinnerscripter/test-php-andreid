<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">

		<div class="row paddinger">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h4 class="paddinger">Add E-mail to List</h4></div>
					<div class="panel-body">
						<form class="form" method="post">
                    		<div class="form-group col-md-5">
                    			<label for="email">E-mail:</label>
                    			<input type="text" name="email" id="email" class="form-control" placeholder="ex.: yourname@company.com" required="required">
                    		</div>
                    		<div class="form-group col-md-5">
                    			<label for="namesurname">Company or Name/Surname</label>
                    			<input type="text" name="namesurname" id="namesurname" class="form-control" placeholder="ex.: Elecrointel SRL" required="required">
                    		</div>
                    		<div class="form-group col-md-2 text-center">
                    			<button type="submit" name="add_mailing" class="btn btn-default" style="margin-top:22px;">ADD</button>
                    		</div>
                    	</form>
                    	<div class="clearfix"></div>
						<?
							if($_SERVER['REQUEST_METHOD']==='POST' && isset($_POST['add_mailing'])){ echo $success;	}
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="row paddinger">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h4 class="paddinger">Bulk Add E-mail to List</h4></div>
					<div class="panel-body">
						<form class="form" method="post">
                    		<div class="form-group col-xs-12">
                    			<label for="email">E-mail:</label><br><small>* Enter the list in the format shown below.</small>
                    			<textarea rows="5" name="bulklist" placeholder="yourname@company.com, Elecrointel SRL&#13;&#10;yourname2@company.com, John Doe&#13;&#10;comp3@company.com, Company Name" class="form-control" required="required"></textarea>
                    		</div>
                    		<div class="form-group col-xs-12 text-center">
                    			<button type="submit" name="add_bulk" class="btn btn-default" style="margin-top:22px;">ADD BULK LIST</button>
                    		</div>
                    	</form>
                    	<div class="clearfix"></div>
						<?
							if($_SERVER['REQUEST_METHOD']==='POST' && isset($_POST['add_bulk'])){ print_r($vk);	}
						?>
					</div>
				</div>
			</div>
		</div>

		<div class="row paddinger">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h4 class="paddinger">Pending E-mails</h4>
						
					</div>
					<div class="panel-body">
                    	<div class="table-responsive">
							<table class="table table-striped"  id="sortable-pending">
								<thead>
									<tr>
										<th class="center">Nr.</th>
										<th class="center">Satus</th>
										<th>E-mail</th>
										<th>Name/Surname</th>
										<th class="center">Delete</th>
									</tr>
								</thead>
								<tbody>
									<? 
									$i=1;
									foreach($pending_invitations as $item){ ?>
									<tr id="arrayorder_<?php echo $item['id'] ?>">
										<td><? echo $i; ?></td>
										<td class="center" style="color:red;font-size:22px;">&bull;</td>
										<td><? echo html_entity_decode($item['email']); ?></td>
										<td><? echo html_entity_decode($item['namesurname']); ?></td>
										<td class="center remover"><a href="<? echo ADMIN_URL ?>mailing?action=delete_mail&amp;id=<? echo $item['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete E-mail from Server" onClick="return confirm('Are you sure you want to delete this E-mail record?');"><i class="ion-ios-close-empty"></i></td>
									</tr>
									<? $i++; } ?>
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>

		<div class="row paddinger">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"><h4 class="paddinger">Sent E-mails</h4>
						
					</div>
					<div class="panel-body">
                    	<div class="table-responsive">
							<table class="table table-striped">
								<thead>
									<tr>
										<th class="center">Nr.</th>
										<th class="center">Satus</th>
										<th class="center">Sent on Date/Time</th>
										<th>E-mail</th>
										<th>Name/Surname</th>
										<th class="center">Resend</th>
										<th class="center">Delete</th>
									</tr>
								</thead>
								<tbody>
									<? 
									$i=1;
									foreach($sent_invitations as $item){ ?>
									<tr>
										<td class="center"><? echo $i; ?></td>
										<td class="center" style="color:green;font-size:22px;">&bull;</td>
										<td class="center"><? echo $item['datetime']; ?></td>
										<td><? echo html_entity_decode($item['email']); ?></td>
										<td><? echo html_entity_decode($item['namesurname']); ?></td>
										<td class="center editor"><a href="<? echo ADMIN_URL ?>mailing?action=reque_mail&amp;id=<? echo $item['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Resend e-mail Letter. Make pending."><i class="ion-ios-refresh-empty"></i></td>
										<td class="center remover"><a href="<? echo ADMIN_URL ?>mailing?action=delete_mail&amp;id=<? echo $item['id']; ?>" data-toggle="tooltip" data-placement="bottom" title="Delete E-mail from Server" onClick="return confirm('Are you sure you want to delete this E-mail record?');"><i class="ion-ios-close-empty"></i></td>
									</tr>
									<? $i++; } ?>
								</tbody>
							</table>
						</div>
					
					</div>
				</div>
			</div>
		</div>


	</div>
</div>

<?
include_once('footer.php'); 
?>