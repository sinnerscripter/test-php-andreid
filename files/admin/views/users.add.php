<?
include_once('head.php'); 
include_once('main_header.php'); 
?>
<div class="row">
	<? include_once('left.php'); ?>
	<div class="col-md-10 right" style="background:#ebeff7">
		<div class="row paddinger">
		


		<div class="col-md-12">
				<div class="panel panel-default">
				<div class="panel-heading"><h4 class="paddinger">Add New user</h4></div>
				<div class="panel-body">
				<div style="color:red;"><? if(isset($_GET['error'])){echo $_GET['error'];} ?> </div>
				
				<form role="form" class="form" method="post">
					
					<div class="form-group col-lg-6">
						<label for="name" style="min-width:150px;">Name/Surname:</label>
						<input type="hidden" name="action" value="register_new_user">
						<input type="text" class="form-control" id="name" name="name">
				  	</div>
					<div class="form-group col-lg-6">
						<label for="username" style="min-width:150px;">E-mail (Username):</label>
						<input type="email" class="form-control" id="username" name="username" required="required" >
				  	</div>
				  	<div class="form-group col-lg-6">
						<label for="phone" style="min-width:150px;">Phone:</label>
						<input type="text" class="form-control" id="Phone" name="phone" required="required" >
				  	</div>
					<div class="form-group col-lg-6 ">
						<label for="pass" style="min-width:150px;">Password:</label>
						<input type="password" class="form-control" id="pass" name="pass" required>
				  	</div>
					<div class="form-group col-lg-6 ">
						<label for="role" style="min-width:150px;">Role:</label>
						<select id="role" name="role" class="form-control" style="min-width:150px;">
							<option value="admin">Administrator</option>
							<option value="editor">Editor</option>
							<option value="visitor">Visitor</option>
						</select>
				  	</div>
				  	<div class="form-group col-lg-6 ">
						<label for="status" style="min-width:150px;">Status:</label>
						<select id="status" name="status" class="form-control" style="min-width:150px;">
							<option value="1">Active</option>
							<option value="0">Blocked</option>
						</select>
				  	</div>
                    
					<div class="form-group col-xs-12 paddinger center">
						<button type="submit" class="btn btn-default" name="add_user">Register User</button>
					</div>
				</form>
				</div>
			</div>
			</div>



		</div>
	</div>
</div>

<?
include_once('footer.php'); 
?>