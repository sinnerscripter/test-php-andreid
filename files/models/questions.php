<?
Class Questions{

	
	public function getQuestions() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `questions` ORDER BY `id` DESC", array());
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

    public function getQuestion($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
           
            $getrow = $database->getRow("SELECT * FROM questions WHERE id=?", array($id));
            $database->Disconnect();
            return $getrow;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

  
    

    public function deleteQuestion($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $deleterow = $database->deleteRow("DELETE FROM `questions` WHERE id = ?", array($id));
            $database->Disconnect();
            return $deleterow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function addQuestion($title, $response_1, $response_2, $response_3, $correct) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO `questions` (`title`, `response_1`, `response_2`, `response_3`, `correct`) VALUES (?, ?, ?, ?, ?)", array($title, $response_1, $response_2, $response_3, $correct)); 
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

  

}
?>