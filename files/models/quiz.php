<?
Class Quizes{

	
	public function getQuizes() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `quizes` ORDER BY `id` DESC", array());
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

    public function getQuiz($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
           
            $getrow = $database->getRow("SELECT * FROM quizes WHERE id=?", array($id));
            $database->Disconnect();
            return $getrow;
            

        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    

    public function deleteQuiz($id) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $deleterow = $database->deleteRow("DELETE FROM `quizes` WHERE id = ?", array($id));
            $database->Disconnect();
            return $deleterow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function addQuiz($title, $allquestions) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO `quizes` (`title`, `allquestions`) VALUES (?, ?)", array($title, $allquestions, )); 
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }

    public function getQuestions() {
            $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
            try{
                $getrows = $database->getRows("SELECT * FROM `questions` ORDER BY `id` ASC", array());
                $database->Disconnect();
                return $getrows;
            }catch(PDOException $e){
                    throw new Exception($e->getMessage());
            }
    }

public function addTest($name_surname, $all_test) {
        $database = new DBPDO("".DB_USER."", "".DB_PASS."", "".DB_HOST."", "".DB_NAME."");  
        try{
            $insertrow = $database ->insertRow("INSERT INTO `tests` (`name_surname`, `answers`) VALUES (?, ?)", array($name_surname, $all_test)); 
            $database->Disconnect();
            return $insertrow;
        }catch(PDOException $e){
                throw new Exception($e->getMessage());
        }

    }
    

}
?>