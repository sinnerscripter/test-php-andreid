-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 03, 2021 at 05:44 PM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 7.1.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `assist`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(255) NOT NULL,
  `title` longtext NOT NULL,
  `response_1` longtext NOT NULL,
  `response_2` longtext NOT NULL,
  `response_3` longtext NOT NULL,
  `correct` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `title`, `response_1`, `response_2`, `response_3`, `correct`) VALUES
(8, 'Intrebarea Unu?', 'Raspuns unu_un', 'Raspuns unu_doi', 'Raspuns unu_trei', '2,3'),
(9, 'INtrebarea doi?', 'Raspuns doi_un', 'Raspuns doi_doi', 'Raspuns doi_trei', '1,3'),
(10, 'INtrebarea trei', 'Raspuns trei_unu', 'Raspuns trei_doi', 'Raspuns trei_trei', '1');

-- --------------------------------------------------------

--
-- Table structure for table `quizes`
--

CREATE TABLE `quizes` (
  `id` int(255) NOT NULL,
  `title` varchar(5000) NOT NULL,
  `allquestions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quizes`
--

INSERT INTO `quizes` (`id`, `title`, `allquestions`) VALUES
(2, 'Quiz Unu', '8,10'),
(3, 'Quiz Doi', '9,10'),
(4, 'Quiz Trei', '8,9,10');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(255) NOT NULL,
  `title_en` text NOT NULL,
  `title_ro` text NOT NULL,
  `title_ru` text NOT NULL,
  `meta_description_en` text NOT NULL,
  `meta_description_ro` text NOT NULL,
  `meta_description_ru` text NOT NULL,
  `meta_keywords_en` text NOT NULL,
  `meta_keywords_ro` text NOT NULL,
  `meta_keywords_ru` text NOT NULL,
  `google_analytics_code` text NOT NULL,
  `maintenance` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title_en`, `title_ro`, `title_ru`, `meta_description_en`, `meta_description_ro`, `meta_description_ru`, `meta_keywords_en`, `meta_keywords_ro`, `meta_keywords_ru`, `google_analytics_code`, `maintenance`) VALUES
(1, 'Assist', 'Assist', 'Assist', 'Assist', 'Assist', 'Assist', '', '', '', '&lt;script&gt;\r\n//google code\r\n&lt;/script&gt;', '');

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(255) NOT NULL,
  `name` text NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `value`) VALUES
(1, 'active', '1'),
(2, 'blocked', '0'),
(3, 'pending', '2');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE `tests` (
  `id` int(255) NOT NULL,
  `name_surname` varchar(5000) NOT NULL,
  `answers` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `name_surname`, `answers`) VALUES
(1, 'Andy Dor', '[{\"question_id\":\"8\",\"answers\":\"2,3\"},{\"question_id\":\"10\",\"answers\":\"1,2\"}]'),
(2, 'ef wefwef', '[{\"question_id\":\"8\",\"answers\":\"3\"},{\"question_id\":\"10\",\"answers\":\"2,3\"}]'),
(3, '', '[{\"question_id\":\"8\",\"answers\":\"1\"},{\"question_id\":\"9\",\"answers\":\"2\"},{\"question_id\":\"10\",\"answers\":\"3\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(11) NOT NULL,
  `qued_text` text NOT NULL,
  `trans_en` text NOT NULL,
  `trans_ro` text NOT NULL,
  `trans_ru` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `qued_text`, `trans_en`, `trans_ro`, `trans_ru`) VALUES
(4, 'Settings', 'Settings', 'SetÄƒri', 'ÐÐ°ÑÑ‚Ñ€Ð¾Ð¹ÐºÐ¸'),
(5, 'Translation', 'Translation', 'Traducere', 'ÐŸÐµÑ€ÐµÐ²Ð¾Ð´'),
(9, 'blocked', 'Blocked', 'Blocat', 'Ð—Ð°Ð±Ð»Ð¾ÐºÐ¸Ñ€Ð¾Ð²Ð°Ð½'),
(10, 'active', 'Active', 'Activ', 'ÐÐºÑ‚Ð¸Ð²ÐµÐ½'),
(11, 'Surname', 'Surname', 'Prenume', 'Ð¤Ð°Ð¼Ð¸Ð»Ð¸Ñ'),
(12, 'Login', 'Login', 'Logare', 'Ð’Ñ…Ð¾Ð´'),
(13, 'Registration', 'Registration', 'ÃŽnregistrare', 'Ð ÐµÐ³Ð¸ÑÑ‚Ñ€Ð°Ñ†Ð¸Ñ'),
(14, 'E-mail', 'E-mail', 'E-mail', 'Ð•-Ð¼Ð°Ð¹Ð»'),
(15, 'Password', 'Password', 'Parola', 'ÐŸÐ°Ñ€Ð¾Ð»ÑŒ'),
(16, 'Register', 'Register', 'ÃŽnregistrare', 'Ð—Ð°Ñ€ÐµÐ³Ð¸ÑÑ‚Ñ€Ð¸Ñ€Ð¾Ð²Ð°Ñ‚ÑŒÑÑ'),
(17, 'Name', 'Name', 'Nume', 'Ð˜Ð¼Ñ'),
(18, 'Username', 'Username', 'Nume utilizator', 'Ð˜Ð¼Ñ Ð¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÐµÐ»Ñ'),
(21, 'You are a Bot', 'You are a Bot', 'RoboÈ›ii nu sunt permiÈ™i', 'Ð‘Ð¾Ñ‚Ñ‹ Ð½Ðµ Ð´Ð¾Ð¿ÑƒÑÐºÐ°ÑŽÑ‚ÑÑ'),
(22, 'Please confirm your password', 'Please confirm your password', 'ConfirmaÈ›i parola', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð¿Ð¾Ð´Ñ‚Ð²ÐµÑ€Ð´Ð¸Ñ‚Ðµ Ð²Ð°Ñˆ Ð¿Ð°Ñ€Ð¾Ð»ÑŒ'),
(23, 'Please insert your password', 'Please insert your password', 'IntroduceÈ›i parola', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ð²Ð°Ñˆ Ð¿Ð°Ñ€Ð¾Ð»ÑŒ'),
(24, 'Please insert your e-mail address', 'Please insert your e-mail address', 'IntroduceÈ›i adresa dvs. de e-mail', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ ÑÐ²Ð¾Ð¹ Ð°Ð´Ñ€ÐµÑ ÑÐ»ÐµÐºÑ‚Ñ€Ð¾Ð½Ð½Ð¾Ð¹ Ð¿Ð¾Ñ‡Ñ‚Ñ‹'),
(25, 'Please insert your username', 'Please insert your username', 'IntroduceÈ›i numele dvs. de utilizator', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ð¸Ð¼Ñ Ð¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÐµÐ»Ñ'),
(26, 'Please insert your surname', 'Please insert your surname', 'IntroduceÈ›i numele dvs. de familie', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ð²Ð°ÑˆÑƒ Ñ„Ð°Ð¼Ð¸Ð»Ð¸ÑŽ'),
(27, 'Please insert your name', 'Please insert your name', 'IntroduceÈ›i numele dvs.', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ð²Ð°ÑˆÐµ Ð¸Ð¼Ñ'),
(28, 'Send', 'Send', 'Trimite', 'ÐžÑ‚Ð¿Ñ€Ð°Ð²Ð¸Ñ‚ÑŒ'),
(41, 'Such user already exists', 'Such user already exists', 'Acest utilizator existÄƒ deja', 'Ð¢Ð°ÐºÐ¾Ð¹ Ð¿Ð¾Ð»ÑŒÐ·Ð¾Ð²Ð°Ñ‚ÐµÐ»ÑŒ ÑƒÐ¶Ðµ ÑÑƒÑ‰ÐµÑÑ‚Ð²ÑƒÐµÑ‚'),
(46, 'Username or Password is Incorrect', 'Username or Password is Incorrect', 'Adresa e-mail sau parola sunt incorecte', 'Ð­Ð»ÐµÐºÑ‚Ñ€Ð¾Ð½Ð½Ð°Ñ Ð¿Ð¾Ñ‡Ñ‚Ð° Ð¸Ð»Ð¸ Ð¿Ð°Ñ€Ð¾Ð»ÑŒ Ð½ÐµÐ²ÐµÑ€Ð½Ñ‹'),
(47, 'Logout', 'Logout', 'IeÈ™ire', 'Ð’Ñ‹Ñ…Ð¾Ð´'),
(52, 'Status', 'Status', 'Statut', 'Ð¡Ñ‚Ð°Ñ‚ÑƒÑ'),
(82, 'Tel', 'Tel', 'Tel', 'Ð¢ÐµÐ»'),
(83, 'Phone', 'Phone', 'Telefon', 'Ð¢ÐµÐ»ÐµÑ„Ð¾Ð½'),
(84, 'Subject', 'Subject', 'Subiect', 'Ð¢ÐµÐ¼Ð°'),
(85, 'Please insert your contact phone', 'Please insert your contact phone', 'IntroduceÈ›i telefonul de contact', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ð²Ð°Ñˆ ÐºÐ¾Ð½Ñ‚Ð°ÐºÑ‚Ð½Ñ‹Ð¹ Ñ‚ÐµÐ»ÐµÑ„Ð¾Ð½'),
(86, 'Please insert a subject', 'Please insert a subject', 'IntroduceÈ›i un subiect', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ñ‚ÐµÐ¼Ñƒ'),
(92, 'Subscribe to Newsletter', 'Subscribe to Newsletter', 'Abonare la Newsletter', 'ÐŸÐ¾Ð´Ð¿Ð¸ÑÐ°Ñ‚ÑŒÑÑ Ð½Ð° Ð½Ð¾Ð²Ð¾ÑÑ‚Ð¸'),
(95, 'This email already exists!', 'This email already exists!', 'Acest email deja existÄƒ!', 'This email already exists!'),
(96, 'Please input a valid email address.', 'Please input a valid email address.', 'IntroduceÈ›i o adresÄƒ de e-mail validÄƒ.', 'ÐŸÐ¾Ð¶Ð°Ð»ÑƒÐ¹ÑÑ‚Ð°, Ð²Ð²ÐµÐ´Ð¸Ñ‚Ðµ Ð¿Ñ€Ð°Ð²Ð¸Ð»ÑŒÐ½Ñ‹Ð¹ Ð°Ð´Ñ€ÐµÑ ÑÐ»ÐµÐºÑ‚Ñ€Ð¾Ð½Ð½Ð¾Ð¹ Ð¿Ð¾Ñ‡Ñ‚Ñ‹.'),
(97, 'Language', 'Language', 'Limba', 'Ð¯Ð·Ñ‹Ðº');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(255) NOT NULL,
  `sort` int(255) NOT NULL,
  `status` text NOT NULL,
  `name` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `image_thumb` text NOT NULL,
  `role` varchar(255) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `psalt` text CHARACTER SET utf32 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `sort`, `status`, `name`, `phone`, `image_thumb`, `role`, `username`, `password`, `psalt`) VALUES
(4, 0, '1', 'Andrei DOROGAN', '', 'uploads/14232454_1449891535026637_2142301302810069248_n.jpg', 'admin', 'info@neoadvanced.com', '8fe8e1cbc9c385557ae8877a3edefaad0b14ad095bef3c0abe49c910b90a3651', 'sN89NnTnA3MbiRdogOhP'),
(418, 0, '1', 'Test User', '+37379518887', '', 'admin', 'test@test.com', 'fc9ea083390873553978766ad5bc8e2be9a09b9e0b0c79b3ed0d4b2553508248', 'tG8bTngkX8rGABs9bwKo');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quizes`
--
ALTER TABLE `quizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `quizes`
--
ALTER TABLE `quizes`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=419;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
